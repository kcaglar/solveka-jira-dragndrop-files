package tr.com.solveka.jira.sjddi.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.jira.config.properties.ApplicationProperties;
/**
 * URLS must follow this pattern
 * create  POST /urlOfRestResource
 * read  GET /urlOfRestResource/path
 * update  PUT /urlOfRestResource/id
 * delete  DELETE /urlOfRestResource/id
 */
@Path ("/applicationProps")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class ApplicationPropertiesResource
{

	ApplicationProperties applicationProperties;
	CacheControl cacheControl;
	
	public ApplicationPropertiesResource(ApplicationProperties applicationProperties) {
        CacheControl cacheControl = new CacheControl();
        cacheControl.setNoCache(true);
        this.applicationProperties = applicationProperties;
    }
 
    @GET
    @Path ("/maxFileSize")
	public Response getMaxPermittedFileSize() {
    	String maxSize = applicationProperties.getString("webwork.multipart.maxSize");
    	return Response.ok(maxSize).cacheControl(cacheControl).build();
    }
}