AJS.$(function () {

    console.log("DnD images plugin js loaded");

    // Replace the jira trigger with this version that logs the events to console. Thanks Chris for writing this!
    JIRA.trigger = (function (trigger) {
        return function () {
            console.log(arguments);
            trigger.apply(this, arguments);
        };
    }(JIRA.trigger));

    var editBoxSelector = "#description-val";
    var commentBoxSelector = "#addcomment";
//    var sdCommentBoxSelector = ""
    
    var availableExtensions = ["jpg", "jpeg", "png", "tif", "tiff", "bmp", "gif"];

    // Hacky...
    var droppedContainer = null;
    // SOLVEKA-EDIT -- hack for preventing upload process from excecuting more than once
    var uploadDone = false;

    function getDescriptionEditBox() {
        return AJS.$(editBoxSelector);
    }

    function getCommentEditBox() {
        return AJS.$(commentBoxSelector);
    }

    //Moves the cursor to the end of a text area and focuses it
    function moveToEnd(textArea){
        textArea.setSelectionRange(textArea.text().length, textArea.text().length);


    }

    function attachDragAndDropHandlers(editBox) {
        editBox.on('dragover',
            function (e) {
                e.preventDefault();
                e.stopPropagation();
            }
        );

        editBox.on('dragenter',
            function (e) {
                e.preventDefault();
                e.stopPropagation();

                var $wikiEdit = AJS.$(this).find(".wiki-edit");
                var overlayContainer = AJS.$("<div />", {"class": "drop-overlay"});
                var overlay = AJS.$("<div />", {"class": "overlay"});
                if ($wikiEdit.length > 0) {
                	 overlayContainer.appendTo($wikiEdit);
                     overlay.appendTo($wikiEdit);
                     AJS.$("<span />", {"class": "overlay-text"}).text(AJS.I18n.getText("sjddi.attachment.drop")).appendTo(overlayContainer);
                     AJS.$(this).find(".wiki-edit textarea").addClass("hidden-border");
                } else {
                	var $sdCommentEdit = AJS.$(this).find(".sd-comment-form-container");
                	if ($sdCommentEdit.length > 0) {
                		overlayContainer.appendTo($sdCommentEdit);
                        overlay.appendTo($sdCommentEdit);
                        AJS.$("<span />", {"class": "overlay-text"}).text(AJS.I18n.getText("sjddi.attachment.drop")).appendTo(overlayContainer);
                        AJS.$(this).find(".sd-comment-form-container textarea").addClass("hidden-border");
                	}
                }

                overlayContainer.hide().addClass("glowing-border").fadeTo("fast", 1.0);
                overlay.hide().fadeTo("slow", 0.2);

            }
        );

        editBox.on('dragleave',
            function(e) {

                e.preventDefault();
                e.stopPropagation();
                var $wikiEdit = AJS.$(this).find(".wiki-edit");
                if ($wikiEdit.length > 0) {
                	$wikiEdit.removeClass("glowing-border");
                	$wikiEdit.find("textarea").removeClass("hidden-border");
                } else {
                	var $sdCommentEdit = AJS.$(this).find(".sd-comment-form-container");
                	if ($sdCommentEdit.length > 0) {
                		$sdCommentEdit.removeClass("glowing-border");
                		$sdCommentEdit.find("textarea").removeClass("hidden-border");
                	}
                }
                AJS.$(this).find(".drop-overlay").remove();
                AJS.$(this).find(".overlay").remove();


            }
        );

        editBox.on('drop',
            function (e) {
                if (e.originalEvent.dataTransfer) {
                    if (e.originalEvent.dataTransfer.files.length) {
                        e.preventDefault();
                        e.stopPropagation();

                        // Remove the "drop images here" overlay
                        var $wikiEdit = AJS.$(this).find(".wiki-edit");
                        if ($wikiEdit.length > 0) {
                        	$wikiEdit.removeClass("glowing-border");
                        	$wikiEdit.find("textarea").removeClass("hidden-border");
                        } else {
                        	var $sdCommentEdit = AJS.$(this).find(".sd-comment-form-container");
                        	if ($sdCommentEdit.length > 0) {
                        		$sdCommentEdit.removeClass("glowing-border");
                        		$sdCommentEdit.find("textarea").removeClass("hidden-border");
                        	}
                        }
                        
                        AJS.$(this).find(".drop-overlay").remove();

                        // Upload files...
                        upload(e.originalEvent.dataTransfer.files, "#" + editBox.attr("id"));
                    }
                }
            }
        );
    }

    attachDragAndDropHandlers(getDescriptionEditBox());
    attachDragAndDropHandlers(getCommentEditBox());

    // We want to always add the handlers after refreshing
    JIRA.bind(JIRA.Events.ISSUE_REFRESHED, function (){
        attachDragAndDropHandlers(getDescriptionEditBox());
        attachDragAndDropHandlers(getCommentEditBox());
    });

    JIRA.bind(JIRA.Events.PANEL_REFRESHED, function (e, panel, element) {
        console.log("panel refreshed");
        console.log(element);
        if (element.is("#descriptionmodule")) {
            attachDragAndDropHandlers(AJS.$(element).find(editBoxSelector));
        } else if (element.is("#addcomment")){
            attachDragAndDropHandlers(element);
        }
    });

    // Called when a file has been dragged on top of an event with the correct handlers
    function upload(files, uploadFromSelector) {
    	if (!uploadDone) {
	        var uploadRequests = [];
	        var failedRequests = [];
	        var notPermittedRequests = [];
	        var maxFileSizeExceeded = false;
	     // Add a "progress" overlay
            
	        var overlayContainer = AJS.$("<div />", {"class": "drop-overlay"});
	        var overlay = AJS.$("<div />", {"class": "overlay"})
            var $container = AJS.$(uploadFromSelector);
            var $wikiEdit = $container.find(".wiki-edit");
            if ($wikiEdit.length > 0) {
            	overlayContainer.appendTo($wikiEdit);
                overlay.appendTo($wikiEdit);
                AJS.$("<span />", {"class": "overlay-text"}).text(AJS.I18n.getText("sjddi.attachment.uploading")).appendTo(overlayContainer);
                $wikiEdit.find("textarea").addClass("hidden-border");
            } else {
            	var $sdCommentEdit = $container.find(".sd-comment-form-container");
            	if ($sdCommentEdit.length > 0) {
            		overlayContainer.appendTo($sdCommentEdit);
                    overlay.appendTo($sdCommentEdit);
                    AJS.$("<span />", {"class": "overlay-text"}).text(AJS.I18n.getText("sjddi.attachment.uploading")).appendTo(overlayContainer);
                    $sdCommentEdit.find("textarea").addClass("hidden-border");
            	}
            }
            
            

            AJS.$("<div />", {"class": "overlay-spinner-container"}).appendTo(overlayContainer).spin();

            overlayContainer.hide().addClass("solid-border").fadeTo("fast", 1.0);
            overlay.hide().fadeTo("slow", 0.2);
            
            // SOLVEKA-EDIT -- get maximum permitted file size from system 
            var maxPermittedFileSize = AJS.$.ajax({
                url: AJS.contextPath() + '/rest/sjddi/1/applicationProps/maxFileSize',                
                async: false,
                type: 'GET'
            });
            
	        AJS.$.each(files, function(index, file) {
	        		            
	            // SOLVEKA-EDIT -- check if file is an image
	            var fileSize = file['size'];
	            
	            var isSuccess = false;
	            if (parseInt(fileSize) <= parseInt(maxPermittedFileSize.responseText)) {
	            	var formData = new FormData(); 
	            	formData.append('file', file);
		            var addAttachmentRequest = AJS.$.ajax({
		                url: AJS.contextPath() + '/rest/api/2/issue/' + JIRA.Issue.getIssueId() + '/attachments',
		                data: formData,
		                cache: false,
		                contentType: false,
		                async: false,
		                headers: {
		                    "X-Atlassian-Token": "nocheck"
		                },
		                processData: false,
		                type: 'POST',
		                statusCode: {
		                	200: function() {
		                		isSuccess = true;
		                	}
		                }
		            });
		            if (isSuccess) {
		            	uploadRequests.push(addAttachmentRequest);
		            } else {
		            	failedRequests.push(file['name']);
		            }
	            } else {
	            	notPermittedRequests.push(file['name']);
	            }
	                      
   
	        });
	
	        // Wait until ALL uploadRequests are done
	        AJS.$.when.apply(AJS.$, uploadRequests).then(function(schemas) {
	
	            var $this = AJS.$(this);          
	            var $textArea = $container.find("textarea");
	            var oldText = $textArea.val();
	
	            // SOLVEKA-EDIT -- present the file in correct notation (if image, show its thumbnail, if not, show link of the document)
	            var markupTags = AJS.$.map(uploadRequests, function(req) {
	            	var request = JSON.parse(req.responseText);
	            	var filename = request[0].filename;
	            	var mimeType = request[0].mimeType;
	            	if (AJS.$.inArray(mimeType.split('/')[1], availableExtensions) != -1) {
	            		return "!" + filename + "|thumbnail!";
	            	} else {
	            		return "[^" + filename + "]";
	            	}
	                
	            });
	
	            if (oldText != "") {
	                markupTags.unshift(oldText);
	            }
	
	            newText = markupTags.join("\n");
	
	            // Remove the overlay
	            var $wikiEdit = $container.find(".wiki-edit");
	            if ($wikiEdit.length > 0) {
	            	$wikiEdit.removeClass("glowing-border").removeClass("solid-border");
	            	$wikiEdit.find("textarea").removeClass("hidden-border");
		            
	            } else {
	            	var $sdCommentEdit = $container.find(".sd-comment-form-container");
	            	if ($sdCommentEdit.length > 0) {
	            		$sdCommentEdit.removeClass("glowing-border").removeClass("solid-border");
	            		$sdCommentEdit.find("textarea").removeClass("hidden-border");
	            	}
	            }
	            $container.find(".drop-overlay").remove();
	            $container.find(".overlay").remove();
	            

	            $textArea.blur();
	            
	            // SOLVEKA-EDIT -- hack for preventing upload process from excecuting more than once
	            uploadDone = true;
	
	            // Refresh the issue so we can see the newly uploaded attachments
	            JIRA.trigger(JIRA.Events.REFRESH_ISSUE_PAGE, [JIRA.Issue.getIssueId()]);
	
	            // Wait for the above call to finish
	            JIRA.one(JIRA.Events.ISSUE_REFRESHED, function () {
	
	                // We need to wait for editBox.click() to become available
	                _.defer(function () {	
	                    var uploadEditBox = AJS.$(uploadFromSelector); //just get it again to make sure it's the same thing
	                    if (uploadFromSelector == "#descriptionmodule" || uploadFromSelector == editBoxSelector){ //not entirely sure why || #descriptionmodule but it's 3am ok.
	                        // We need to click on the edit box as we've refreshed the issue panel
	                        uploadEditBox.click();
	
	                        // Wait for the textarea to have focus after the click
	                        JIRA.one(JIRA.Events.INLINE_EDIT_FOCUSED, function () {
	                            var $container = AJS.$(uploadFromSelector);
	                            var $textArea = $container.find("textarea");
	                            var oldText = $textArea.val();
	
	                            // SOLVEKA-EDIT -- present the file in correct notation (if image, show its thumbnail, if not, show link of the document)
	                            var markupTags = AJS.$.map(uploadRequests, function(req) {
	                            	var request = JSON.parse(req.responseText);
	                            	var filename = request[0].filename;
	                            	var mimeType = request[0].mimeType;
	                            	if (AJS.$.inArray(mimeType.split('/')[1], availableExtensions) != -1) {
	            	            		return "!" + filename + "|thumbnail!";
	            	            	} else {
	            	            		return "[^" + filename + "]";
	            	            	}
	                            });
	
	                            if (oldText != "") {
	                                markupTags.unshift(oldText);
	                            }

	                            newText = markupTags.join("\n");
	                            $textArea.val(newText);
	                            moveToEnd($textArea);
	
	                        });
	
	                    } else if (uploadFromSelector == commentBoxSelector){
	                    	// check if comment is being made in SD
	                    	var $sdCommentEdit = AJS.$(".sd-comment-container");
	                    	if ($sdCommentEdit.length > 0) {
	                    		var $sdCommentEditText = $sdCommentEdit.find("textarea");
	                    		$sdCommentEditText.focus();
	                    		// hack to get updated html after focus operation
	                    		$sdCommentEditText = $sdCommentEdit.find("textarea");
	                    		$sdCommentEditText.val(newText);
	                    		moveToEnd($sdCommentEditText);
	                    		
	                    	} else {
	                    		$textArea.val(newText);
	 	                        moveToEnd($textArea);
	                    	}
	                       
	
	                        //Slowly scroll back into view after adding a comment
	                        AJS.$('html, body').animate({
	                            scrollTop: uploadEditBox.find("textarea").offset().top
	                        }, 500);
	                    } else{
	                        console.log("Bound but couldn't match the selector. It was "+uploadFromSelector);
	                    }
	                    uploadDone = false;
                    });
	
	            });
	
	        }, function(jqXHR, textStatus, errorThrown) {
	        	
	            console.log("Ajax request failed");
	        });
	        
	        // SOLVEKA-EDIT -- show error message if upload is unsuccessful
	        if (notPermittedRequests.length > 0 || failedRequests.length > 0) {
		        var msg = "";
		        if (notPermittedRequests.length > 0) {	
		        	AJS.$.each(notPermittedRequests, function(index, value) {
		        		msg = msg + '<p>' + AJS.I18n.getText("sjddi.attachment.error.maxFileSizeExceeded", value) + '</p>'; 	
		        	});
		        }
		        
		        if (failedRequests.length > 0) {
		        	AJS.$.each(failedRequests, function(index, value) {
		        		msg = msg + '<p>' + AJS.I18n.getText("sjddi.attachment.error.general", value) + '</p>'; 	
		        	});
		        }	
		        
		        JIRA.Messages.showErrorMsg(msg, {closeable: true});
	        }	        
    	}   
    }

});